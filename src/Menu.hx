import starling.display.Sprite;
import starling.display.Button;
import starling.display.Image;
import starling.core.Starling;
import starling.textures.Texture;
import starling.events.Event;
import Game;
import Root;
import Std;

class Menu extends Sprite {

	private var mRoot:Root;
	private var menuButtons:Array<MenuButton>; 
    private var menuItems:Array<String>;
    public var buttonWidth(default, null):Int;
	public var buttonHeight(default, null):Int;

	public function new(r:Root, names:Array<String>,color_index:Int, btnWidth:Int=100, btnHeight:Int=30, btnPad:Int=5) {
		super();
		this.mRoot = r;
		this.menuButtons = new Array<MenuButton>();
		this.x = Starling.current.stage.stageWidth/2 - btnWidth/2-100;
		this.menuItems = names;
		this.buttonWidth = btnWidth;
		this.buttonHeight = btnHeight;

        var title_text = new Image(Root.assets.getTexture("colour_title"));
        title_text.alignPivot();
        title_text.x = 300;
        title_text.y = -50;
        addChild(title_text);
		
		for (m in menuItems) {
			createColorButton(Game.palette[(menuItems.indexOf(m)+1+color_index)%Game.palette.length], m); 
		}

		addEvents();

		this.y = Starling.current.stage.stageHeight/2 - menuButtons.length * btnHeight/2;
		
		positionButtons(btnPad);
	}

	public function createColorButton(color:Int, name:String) {
		var t:Texture = Texture.fromColor(buttonWidth, buttonHeight, color + 0xFF000000); //0xAA99099);
		var b:MenuButton = new MenuButton(name, t);
                b.fontName = "Harlow_Solid_Italic_32";
                b.fontSize = 64;
                b.fontBold = true;
		menuButtons.push(b);
		addChild(b);
	}

	public function addEvents() {
		for (b in menuButtons) {
			b.addEventListener(Event.TRIGGERED, function(e:Event) {
				mRoot.floodFill.color = Game.palette[menuButtons.indexOf(b)+1];
				if (b.text == "play") {
					mRoot.level_select();
			    }
			    else if (b.text == "instructions") {
			    	mRoot.instructions();
			    }
				else if (b.text  == "credits") {
				    mRoot.credits();
				}
				
			});
		}
	}

	public function positionButtons(padding:Int) {
		var i:Int = 50;
		for (button in menuButtons) {
			button.y = i;
			i = i + padding + this.buttonHeight;
		}
	}


}

class MenuButton extends Button {
	public function new(name, texture) {
		super(texture, name);
		this.fontSize = 64;
		this.fontBold = true;
		this.fontColor = 0xFFFFFF;
	}
}

