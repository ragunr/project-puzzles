import starling.core.Starling;
import starling.display.Sprite;
import starling.display.Quad;
import starling.display.Image;
import starling.display.Button;
import starling.textures.Texture;
import starling.events.TouchEvent;
import starling.events.TouchPhase;
import starling.events.Event;
import starling.text.TextField;
import starling.utils.HAlign;
import flash.geom.Point;
import flash.geom.Rectangle;
import Std;
import Math;
import Game;

class LevelSelect extends Sprite{
    public var m_root:Root;
    public function new(m_root){
        super();
        this.m_root = m_root;
        var randomizations = 4;
        var levelsPerRandomizations = 4;
        var r = new Random(42424242);

        var palette_per_level = [0,2,3,4];
        var label_text = ["Easy","Medium","Hard","Impossible"];

        for(i in 0...randomizations){
            var label = new TextField(
                    400,
                    100,
                    label_text[i],
                    "Harlow_Solid_Italic_32",
                    32,
                    Game.palette[palette_per_level[i]],
                    true);
            label.alignPivot();
            label.x = 215;
            label.y = 125+40+(90*i);
            addChild(label);
        }

        for(i in 0...randomizations*levelsPerRandomizations){
            //var buttonTex:Texture = Texture.fromColor(100, 20, 0x999999AA);
            //var button = new Button(buttonTex, i+"");
            var button = new Button(Root.assets.getTexture("level_shape"), i+"");
            button.color = Game.palette[palette_per_level[
                Std.int(i/levelsPerRandomizations)]];
            //button.textBounds = new Rectangle(-100,-50,200,100);
            button.textHAlign = HAlign.CENTER;
            button.fontColor = 0xFFFFFF;
            button.fontName = "Harlow Solid Italic";
            button.fontSize = 40;
            button.fontBold = true;
            button.width = 80;
            button.height = 80;
            button.x = 325+(i%levelsPerRandomizations)*90;
            button.y = 125+(90*Math.floor((i/randomizations)));
            var c = r.int();
            button.addEventListener(Event.TRIGGERED, function() {
                    m_root.play(
                        c,
                        Math.floor(i/levelsPerRandomizations)+2);
                });
            addChild(button);
            //trace(button.textBounds);
            //trace(button.textHAlign);
        }

        var back_arrow = new Image(Root.assets.getTexture("back_arrow"));
        back_arrow.alignPivot();
        back_arrow.x = 40;
        back_arrow.y = Starling.current.stage.stageHeight-40;
        addChild(back_arrow);

        back_arrow.addEventListener(TouchEvent.TOUCH, function(e:TouchEvent){
                var touch = e.getTouch(back_arrow,TouchPhase.BEGAN);
                if(touch != null)
                {
                    m_root.start();
                }
                });
    }
}
