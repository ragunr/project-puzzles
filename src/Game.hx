import starling.display.Sprite;
import starling.display.Quad;
import starling.display.Image;
import starling.events.Event;
import starling.events.EnterFrameEvent;
import starling.events.TouchEvent;
import starling.events.TouchPhase;
import starling.text.TextField;
import starling.core.Starling;
import starling.animation.Tween;
import starling.animation.Transitions;
import flash.geom.Point;
import starling.animation.Tween;
import starling.animation.Transitions;
import Std;
import Math;
import Root;
import ColorTween;

class Game extends Sprite{
    public var m_root : Root;
	public var gamephase : GamePhase = PRE_GAME;
	public var input_click:Bool = true;
    public var controls:Sprite;
    public var moves_counter:TextField;

    // Width and height of the canvas. May be different than the computed 
    // height and width properties
    public var w : Int;
    public var h : Int;

    // Big shape dimensions and padding
    public var shapesize : Float;
    public var shapepadding : Float;

    // Small shape dimensions and padding
    public var smallshapesize : Float;
    public var smallshapepadding : Float;

    // Color categories. right now there are 5.
    // TODO: Replace this with an enum class
    public static inline var CATEGORY_COUNT = 5;
    public static var palette_test = {
        [0xAAAAFF, 0xAAFFAA, 0xFFAAAA, 0xFFAAFF, 0xAAFFFF];
    }
    public static var palette_popiseverything = {
        [0xAAFF00, 0xFFAA00, 0xFF00AA, 0xAA00FF, 0x00AAFF];
    }
    public static var palette_sweetlolly = {
        [0xFF003C, 0xFF8A00, 0xFABE28, 0x88C100, 0x00C176];
    }
    public static var palette_mellow = {
        [0x11644D, 0xA0B046, 0xF2C94E, 0xF78145, 0xF24E4E];
    }
    public static var palette_creamyrainbow = {
        [0xFA3C68, 0xF5FA53, 0x9CFA1E, 0x1EE5FA, 0xE91EFA];
    }
    public static var palette = palette_creamyrainbow;

    // gamestate and shapelist both correspond to one another in indexes
    // 0 - bg, 1 - container 1, 2 - container 1 shape 1,
    // 3 - container 1 shape 2, etc...
    // gamestate holds the numeric value for each shape
    public var gamestate : Array<Int>;
    // shapelist tells us where the quad/images are to modify
    public var shapelist : Array<Quad>;

    public var moves:Int = 0;

    // A random object we can seed oursevles
    private var random : Random;

    public function new(width,height,m_root, level_seed, difficulty){
        // level_seed is currently = to # of Randomizations
        // remember dimension
        this.w = width;
        this.h = height;
        this.m_root = m_root;
        super();

        // determine our shapesize based on screensize
        shapesize = height/12*5;
        shapepadding = height/12*2;

        smallshapesize = shapesize/5*2;
        smallshapepadding = shapesize/5*1;

        // randomize initial setup.
        random = new Random(level_seed);

        // Right now we are initializing the board to the first color
        // so we can randomize it later. Once we have fun rules we will
        // want to initialize to a random color
        var start_color = random.int() % CATEGORY_COUNT;
        gamestate = new Array<Int>();
        for(i in 0...21) {
            gamestate.push(start_color);
        }

        // This builds the geometry and populate shapelist
        initialize_shapes();

        // Wire all the shapes for touch events.
        for(i in 0...shapelist.length){
            var shape = shapelist[i];
            shape.addEventListener(TouchEvent.TOUCH, function(e:TouchEvent){
						//trace(input_click);
                        if(input_click == false) return;
						var touch = e.getTouch(this,TouchPhase.BEGAN);
						if(touch != null) 
                        {
                            var index = shapelist.indexOf(shape);
                            //trace(index);
                            // Use a radius test, and send our touch to
                            // the parent if the radius test fails
                            var radius = Point.distance(
                                touch.getLocation(shape),
                                new Point(shape.pivotX,shape.pivotY));
                            //trace("radius"+radius);
                            var parent_index = get_gamestate_parent_index(index);
                            if(radius < 50 || parent_index <= 0) {
                                touched(index);
                            } else {
                                touched(parent_index);
                            }
                        }
                    });
        }

        this.addEventListener(
                Event.ENTER_FRAME,
                function(e:EnterFrameEvent){this.advanceTime(e.passedTime);});


        for(i in 0...difficulty){
            touched(random.int()%(gamestate.length-1)+1);
        }
		
		gamephase = GAME_PLAY;

        initialize_controls();


        var fade_fill = new Quad(w,h,palette[gamestate[0]]);
        addChild(fade_fill);
        var fade_in = new Tween(fade_fill,2.0,Transitions.EASE_IN_OUT);
        fade_in.fadeTo(0);
        fade_in.delay = 2.0;
        fade_in.onComplete = function () {removeChild(fade_fill);};
        Starling.juggler.add(fade_in);
    }

    public function initialize_shapes() {
        shapelist = new Array<Quad>();

        // Tier 1 target shape
        var quad = build_background_shape(palette[gamestate[0]]);
        shapelist.push(quad);
        quad.x = w/2;
        quad.y = h/2;
        this.addChild(quad);

        // Tier 1 sprite contains all shapes
        var tier1 = new Sprite();
        tier1.x = w/2*1.125;
        tier1.y = h/2;
        tier1.scaleX = 0.89;
        tier1.rotation = Math.PI*2*0.05;
        tier1.scaleY = tier1.scaleX;
        addChild(tier1);



        // Add subtier sprites for each subshape
        for(i in 0...4) {
            // Tier 1 sprite contains a big shape
            var tier2 = new Sprite();
            // It would be cooler if this was done mathmaticly,
            // but this works for now
            if(i==0){
                tier2.x = -shapepadding/2-shapesize/2;
                tier2.y = -shapepadding/2-shapesize/2;
            } else if(i == 1){
                tier2.x = shapepadding/2+shapesize/2;
                tier2.y = -shapepadding/2-shapesize/2;
            } else if(i == 2){
                tier2.x = shapepadding/2+shapesize/2;
                tier2.y = shapepadding/2+shapesize/2;
            } else if(i == 3){
                tier2.x = -shapepadding/2-shapesize/2;
                tier2.y = shapepadding/2+shapesize/2;
            }
            tier1.addChild(tier2);

            // Tier 2 target shape
            var quad = build_outter_shape(palette[gamestate[1+i*5]]);
            shapelist.push(quad);
            tier2.addChild(quad);

            // Add subtier sprites for each subshape
            for(j in 0...4){
                // Tier 3 sprite contains a small shape
                var tier3 = new Sprite();
                tier3.x = -shapesize/2;
                tier3.y = -shapesize/2;
                // Also should be done with the maths
                if(j==0){
                    tier3.x = -smallshapepadding/2-smallshapesize/2;
                    tier3.y = -smallshapepadding/2-smallshapesize/2;
                } else if(j == 1){
                    tier3.x = smallshapepadding/2+smallshapesize/2;
                    tier3.y = -smallshapepadding/2-smallshapesize/2;
                } else if(j == 2){
                    tier3.x = smallshapepadding/2+smallshapesize/2;
                    tier3.y = smallshapepadding/2+smallshapesize/2;
                } else if(j == 3){
                    tier3.x = -smallshapepadding/2-smallshapesize/2;
                    tier3.y = smallshapepadding/2+smallshapesize/2;
                }
                tier2.addChild(tier3);

                // Tier 3 target shape
                var quad = build_inner_shape(palette[gamestate[1+i*5+1+j]]);
                shapelist.push(quad);
                tier3.addChild(quad);

            }
        }
    }

    public function initialize_controls()
    {
        controls = new Sprite();
        addChild(controls);

        var back_arrow = new Image(Root.assets.getTexture("back_arrow"));
        back_arrow.alignPivot();
        back_arrow.x = 40;
        back_arrow.y = h-40;
        controls.addChild(back_arrow);

        back_arrow.addEventListener(TouchEvent.TOUCH, function(e:TouchEvent){
                var touch = e.getTouch(back_arrow,TouchPhase.BEGAN);
                if(touch != null)
                {
                    m_root.level_select();
                }
                });

        var moves_label = new Image(Root.assets.getTexture("moves_label"));
        moves_label.alignPivot();
        moves_label.x = 30;
        moves_label.y = 25;
        controls.addChild(moves_label);

        moves_counter = new TextField(
                100,  // full width
                50,  // full height
                "0", // text
                "Harlow_Solid_Italic_32", // font name TODO: change to game font
                32, // font size
                0xFFFFFF, //font color
                true); // bold
        moves_counter.redraw();
        moves_counter.alignPivot();
        moves_counter.x = 35;
        moves_counter.y = 45;
        controls.addChild(moves_counter);
    }

    public function increment_moves(){
        if(gamephase != GAME_PLAY) return;
        moves += 1;
        moves_counter.text = Std.string(moves);
        moves_counter.redraw();
    }

    public function build_background_shape(color:Int):Quad{
        var shape = new Quad(w,h,color);
        shape.alignPivot();
        return shape;
    }

    public function build_outter_shape(color:Int):Quad{
        var outter_shapes = Root.assets.getTextures("outter_shapes_");
        var shape = new Image(outter_shapes[Std.random(outter_shapes.length)]);
        shape.color = color;
        shape.alignPivot();
        return shape;
    }

    public function build_inner_shape(color:Int):Quad{
        var inner_shapes = Root.assets.getTextures("inner_shapes_");
        var shape = new Image(inner_shapes[Std.random(inner_shapes.length)]);
        shape.color = color;
        shape.alignPivot();
        return shape;
    }

    private function advanceTime(t:Float) {
        shapelist[1].parent.parent.rotation += Math.PI*2*0.00003;
        var osi = 1;
        for(i in 0...3)
        {
            shapelist[osi].rotation -= Math.PI*2*0.00004;
            var ssi = osi+1;
            for(j in 0...3)
            {
                shapelist[ssi].rotation += Math.PI*2*0.00005;
                ssi = get_gamestate_next_index(ssi);
            }
            osi = get_gamestate_next_index(osi);
        }
    }

    public function check_win(){
        var winstate = gamestate[0];
        for(i in 1...21){
            if(gamestate[i] != winstate) return;
        }

        gamephase = END_GAME;
        for(shape in shapelist){
            shape.removeEventListeners(TouchEvent.TOUCH);
        }
        
        var win_text = new Image(Root.assets.getTexture("solved"));
        win_text.alpha = 0;
        win_text.alignPivot();
        win_text.x = w/2;
        win_text.y = h/2-40;
        addChild(win_text);

        var win_moves_counter = new TextField(
                w,  // full width
                h,  // full height
                Std.string(moves)+" Moves", // text
                "Harlow_Solid_Italic_32", // font name TODO: change to game font
                32, // font size
                0xFFFFFF, //font color
                true); // bold
        win_moves_counter.redraw();
        win_moves_counter.alpha = 0;
        win_moves_counter.alignPivot();
        win_moves_counter.x = w/2;
        win_moves_counter.y = h/2 + 20;
        addChild(win_moves_counter);

        var fade_in = new Tween(win_text, 3.0, Transitions.EASE_IN_OUT);
        fade_in.delay = 2.0;
        fade_in.fadeTo(1);
        Starling.juggler.add(fade_in);

        var moves_fade_in = new Tween(win_moves_counter, 3.0, Transitions.EASE_IN_OUT);
        moves_fade_in.delay = 3.0;
        moves_fade_in.fadeTo(1);
        Starling.juggler.add(moves_fade_in);

        moves_fade_in.onComplete = function() {
            this.addEventListener(TouchEvent.TOUCH, function(e:TouchEvent){
                    var touch = e.getTouch(this,TouchPhase.BEGAN);
                    if(touch != null) 
                    {
                        m_root.level_select(); 
                    }
                });
        };

        var fade_controls = new Tween(controls, 1.0, Transitions.EASE_IN_OUT);
        fade_controls.fadeTo(0);
        Starling.juggler.add(fade_controls);


    }
	
    // This is where the mechanics happen.
    public function touched(shapeindex:Int){
        //trace("touched: "+shapeindex);
		

        var middle_index = get_gamestate_parent_index(shapeindex);
        // For debugging index transitions
        //trace(shapeindex + " -> "+middle_index); 

        // throw out touches that wern't small shapes
        if(middle_index < 0) {
            return;
        }
        else if(middle_index == 0) 
        {
            increment_moves();
            shape_rotate(shapeindex,1);
        } 
        else {
            increment_moves();
            color_rotate(0, 1);
            if(middle_index > 0) { color_rotate(middle_index, 1); }
            if(shapeindex != 0) { color_rotate(shapeindex, 1); }
            var next_index = shapeindex;
            for(i in 0...3) { 
                next_index = get_gamestate_next_analogous_index(next_index);
                color_rotate(next_index,1); 
            }
        }

        if(gamephase == GAME_PLAY) {
        	var sound : Int = Std.random(4) + 1;
        	Root.assets.playSound("Chime" + sound);
        }
        if(gamephase == GAME_PLAY) check_win();
    }

    // This actually rotates the colors. Gamestate should change instantly
    // the quad color itself should change pretty.
    public function color_rotate(shapeindex:Int, offset:Int){
        gamestate[shapeindex] = (gamestate[shapeindex]+offset) % CATEGORY_COUNT;
		if(gamephase == GAME_PLAY){
			var color_change = new ColorTween(
                    shapelist[shapeindex], 
                    1.0, 
                    Transitions.EASE_IN_OUT, 
                    shapelist[shapeindex].color, 
                    palette[gamestate[shapeindex]]);

			input_click = false;
			color_change.onComplete = function(){
				input_click = true;
			}
			Starling.juggler.add(color_change);
		}
		else shapelist[shapeindex].color = palette[gamestate[shapeindex]];
		
    }

    public function shape_rotate(shapeindex:Int, offset:Int){
        var child = shapeindex + 1;
        
        var tmp_gamestate = gamestate[child];
        var tmp_shapelist = shapelist[child];

        var next_child:Int;

        for(i in 0...3){
            next_child = get_gamestate_next_index(child);
            //trace("child:"+child);
            gamestate[child] = gamestate[next_child];
            shapelist[child] = shapelist[next_child];

            child = next_child;
        }

        gamestate[child] = tmp_gamestate;
        shapelist[child] = tmp_shapelist;
		
		if(gamephase == GAME_PLAY){
			var shape_spin = new Tween(
                    shapelist[shapeindex].parent, 
                    1.0, 
                    Transitions.EASE_IN_OUT);

			shape_spin.animate(
                    "rotation", 
                    shapelist[shapeindex].parent.rotation - Math.PI/2);

			input_click = false;
			shape_spin.onComplete = function(){
				input_click = true;
			}
			Starling.juggler.add(shape_spin);
		
		}
		else shapelist[shapeindex].parent.rotation -= Math.PI/2;
	}

    public static function get_gamestate_parent_index(index:Int):Int {
        if(index == 0) return -1;
        if((index-1)%5 == 0) return 0;
        return Math.floor((index-1)/5)*5+1;
    }

    public static function get_gamestate_next_index(index:Int):Int {
        if(index == 0) return 0;
        if((index-1)%5 == 0) return (index-1+5)%20+1;
        var parent_index =get_gamestate_parent_index(index) ;
        return (index-parent_index)%4+parent_index+1;
    }

    public static function get_gamestate_prev_index(index:Int):Int {
        for(i in 0...3) index = get_gamestate_next_index(index);
        return index;
    }

    public static function get_gamestate_next_analogous_index(index:Int):Int {
        if(index == 0) return 0;
        if((index-1)%5 == 0) return index;
        return (index-1+5)%20+1;
    }

}

enum GamePhase{
    PRE_GAME;
    GAME_PLAY;
    END_GAME;
}
