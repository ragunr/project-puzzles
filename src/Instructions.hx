import starling.animation.Tween;
import starling.animation.Transitions;
import starling.display.Sprite;
import starling.display.Quad;
import starling.display.Image;
import starling.display.Button;
import starling.textures.Texture;
import starling.events.TouchEvent;
import starling.events.TouchPhase;
import starling.events.Event;
import starling.text.TextField;
import starling.core.Starling;
import flash.geom.Point;
import Std;
import Math;
import Game;

class Instructions extends Sprite{
    public var m_root:Root;
    public var state = 0;
    public function new(m_root){
        super();
        this.m_root=m_root;
        createInstructions();
    }

    function onTouch(e:TouchEvent){
        var touch = e.getTouch(stage,TouchPhase.BEGAN);
        if(touch != null) 
        {
            var fade_out = new Tween(touch.target, 3.0, Transitions.EASE_IN_OUT);
            fade_out.fadeTo(0);
            fade_out.onComplete = function() { touch.target.dispose();removeChildren();state+=1;createInstructions(); };
            Starling.juggler.add(fade_out);
        }
    }

    function createInstructions(){
        switch(state){
            case 0:
                var text = new TextField(400, 300, "Welcome to Colour\n\n    Your Goal is to make the screen one solid color, via clicking the Shapes.\nClick for further Instructions!", "Harlow_Solid_Italic_32", 25, 0x000000, false);
                text.x=(Starling.current.stage.stageWidth / 2) - (text.width / 2);
                text.y=(Starling.current.stage.stageHeight / 2) - (text.height / 2);
                text.addEventListener(TouchEvent.TOUCH, onTouch);
                addChild(text);
            case 1:
                var text = new TextField(800, 200, "There are two Types of Shapes; Inner and Outer\n Inner Shapes are Circles, and Outer Shapes are composed of 4 Inner Shapes.", "Harlow_Solid_Italic_32", 25, 0x000000, false);
                text.x=(Starling.current.stage.stageWidth / 2) - (text.width/2);
                text.y=(Starling.current.stage.stageHeight / 2) - (text.height/2);
                addChild(text);

                var textTween = new Tween(text, 1.0, Transitions.EASE_IN_OUT);
                textTween.delay = 1.5;
                textTween.animate("y", text.y-250);
                textTween.onComplete = function(){
                    var outerText = new TextField(800, 300, "Here's an example of an Outer Shape;", "Harlow_Solid_Italic_32", 25, 0x000000, false);
                    outerText.x = (Starling.current.stage.stageWidth / 2) - (outerText.width/2);
                    outerText.y = text.y+25;
                    addChild(outerText);

                    var outter_shapes = Root.assets.getTextures("outter_shapes_");
                    var shape = new Image(outter_shapes[Std.random(outter_shapes.length)]);
                    shape.color = 0x000000;
                    shape.x = (Starling.current.stage.stageWidth / 2) - (shape.width/2);
                    shape.y = (Starling.current.stage.stageHeight / 2) - (shape.height/2) + 50;
                    addChild(shape);

                    var continueText = new TextField(800, 300, "Click Here to Continue", "Harlow_Solid_Italic_32", 25, 0xFFFFFF, false);
                    continueText.x = (Starling.current.stage.stageWidth / 2) - (continueText.width/2);
                    continueText.y = (Starling.current.stage.stageHeight / 2) - (continueText.height/2) + 50;
                    continueText.addEventListener(TouchEvent.TOUCH, onTouch);
                    addChild(continueText);
                }
                Starling.juggler.add(textTween);
            case 2:
                var text = new TextField(800, 500, "Clicking on an Outer Shape will rotate the Inner Shapes clockwise.\n\nClicking on an Inner Shape will cause it's color, and all of the Inner Shapes in the same position in other Outer Shapes, to change color. Additionally it will change it's Outer Shapes color and the Background's Color.\n\nThus your goal becomes determining how to manipulate the Inner and Outer Shapes so that them and the Background are the same color!\n\nClick to return to Main Menu..", "Harlow_Solid_Italic_32", 25, 0x000000, false);
                text.x=(Starling.current.stage.stageWidth / 2) - (text.width/2);
                text.y=(Starling.current.stage.stageHeight / 2) - (text.height/2);
                text.addEventListener(TouchEvent.TOUCH, onTouch);
                addChild(text);
            case 3:
                m_root.start();
                this.dispose();
        }
    }
}
