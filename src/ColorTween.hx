import starling.animation.IAnimatable;
import starling.animation.Transitions;
import starling.events.EventDispatcher;
import starling.events.Event;
import Std;
import StringTools;

class ColorTween extends EventDispatcher implements IAnimatable{

	private var target:Dynamic;
	private var duration:Float;
	private var transition:Dynamic;
	private var start:Int;
	private var finish:Int;
	private var current_time:Float = 0.0;
	
	public var onComplete:Void -> Void;
	
	public function new(target:Dynamic, duration:Float, transition:Dynamic, start:Int, finish:Int){
		super();
		this.target = target;
		this.duration = duration;
		this.transition = transition;
		this.start = start;
		this.finish = finish;
	}
	
	public function advanceTime(time:Float){
		current_time += time;
		
		if(current_time > duration){
			dispatchEvent(new Event(Event.REMOVE_FROM_JUGGLER));
			onComplete();
		}
		
		var transition_func = Transitions.getTransition(transition);
		
		var progress:Float = transition_func(current_time/duration);
		
		//0x0000FF mask
		var r_val:Int;
		var b_val:Int;
		var g_val:Int;
		
		//start values
		var b_start_val = start & 0x0000FF;
		var g_start_val = (start >>> 8) & 0x0000FF;
		var r_start_val = (start >>> 16) & 0x0000FF;
		
		//end values
		var b_finish_val = finish & 0x0000FF;
		var g_finish_val = (finish >>> 8) & 0x0000FF;
		var r_finish_val = (finish >>> 16) & 0x0000FF;
		
		//middle values
		var b_mid_val = Std.int(b_finish_val * progress + b_start_val * (1 - progress));
		var g_mid_val = Std.int(g_finish_val * progress + g_start_val * (1 - progress));
		var r_mid_val = Std.int(r_finish_val * progress + r_start_val * (1 - progress));
		
		//repacked values
		var repacked_val = r_mid_val;
		repacked_val = repacked_val << 8;
		repacked_val = repacked_val | g_mid_val;
		repacked_val = repacked_val << 8;
		repacked_val = repacked_val | b_mid_val;
		
		target.color = repacked_val;
	}
}