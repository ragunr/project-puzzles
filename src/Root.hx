import starling.display.Sprite;
import starling.display.Quad;
import starling.utils.AssetManager;
import starling.display.Image;
import starling.display.Button;
import starling.events.Event;
import starling.textures.Texture;
import starling.text.TextField;
import starling.core.Starling;
import starling.animation.Transitions;
import starling.animation.Tween;
import starling.display.DisplayObject;
import Game;

class Root extends Sprite {

    public static var WIDTH:Int = 800;
    public static var HEIGHT:Int = 600;

    public static var assets:AssetManager;

    public var backgroundFill : Quad;
    public var floodFill:Quad;
    public var focus : DisplayObject;

    public function new() {
        super();
    }
    public function initialize(startup:Startup) {
        assets = new AssetManager();
        // enqueue here
        assets.enqueue("assets/Harlow_Solid_Italic_32.png");
        assets.enqueue("assets/Harlow_Solid_Italic_32.fnt");
        assets.enqueue("assets/Harlow_Solid_Italic.png");
        assets.enqueue("assets/Harlow_Solid_Italic.fnt");
        assets.enqueue("assets/assets.png");
        assets.enqueue("assets/assets.xml");
        assets.enqueue("assets/serene.mp3");
        assets.enqueue("assets/Chime1.mp3");
        assets.enqueue("assets/Chime2.mp3");
        assets.enqueue("assets/Chime3.mp3");
        assets.enqueue("assets/Chime4.mp3");
        assets.enqueue("assets/Chime5.mp3");

        assets.loadQueue(function onProgress(ratio:Float) {
            if(ratio == 1) {
                Starling.juggler.tween(startup,
                    2.0,
                    {
                        transition: Transitions.EASE_IN,
                        delay: 1.0,
                        alpha: 0,
                        onComplete: function()
                        {
                            startup.removeChild(startup.loadingBitmap);
                            startup.removeChild(startup.loadingBg);
                        }
                    });
                assets.playSound("serene", 0, -1);
                start();
            }
        });
    }

	// TODO -- perhaps create a Menu class to keep Root concise
	// TODO -- add tweening for transitions between scenes
	public function start() {	
            removeChildren();

            backgroundFill = new Quad(WIDTH,HEIGHT,Game.palette[0]);
            addChild(backgroundFill);

            floodFill = new Quad(WIDTH,HEIGHT,Game.palette[0]);
            addChild(floodFill);
		
            var STAGE_CENTER = Starling.current.stage.stageWidth / 2;

            loadMenu(1);
    }

    public function flood_transition(flood_color:Int,callback:Dynamic){
        floodFill.color = flood_color;
        floodFill.alpha = 0;
        setChildIndex(floodFill, numChildren-1);
        var flood_tween = new Tween(floodFill,2.0,Transitions.EASE_IN_OUT);
        flood_tween.fadeTo(1);
        flood_tween.onComplete = function(){
            backgroundFill.color = flood_color;
            floodFill.alpha = 0;
            callback();
        }
        Starling.juggler.add(flood_tween);

    }

    public function play(level_seed, difficulty){ 
        var game = new Game(
                WIDTH,
                HEIGHT,
                this, 
                level_seed, 
                difficulty);
		flood_transition(Game.palette[game.gamestate[0]], function(){ 
                removeChild(focus);
                focus.dispose();
                addChild(game);
                focus = game;
                });
        
    }

    public function level_select(){
		flood_transition(Game.palette[1], function(){
            var levelSelect = new LevelSelect(this);
            removeChild(focus);
            focus.dispose();
            addChild(levelSelect);
            focus = levelSelect;
        }); 
    }

	public function loadMenu(color_index:Int) {
		flood_transition(Game.palette[0], function(){
            var menu:Menu = new Menu( 
                this, 
                ["play", "instructions", "credits"], 
                color_index, 
                cast(WIDTH/5, Int), 
                40);
            if(focus != null){
                removeChild(focus);
                focus.dispose();
            }
		    addChild(menu);
            focus = menu;
        }); 
	}

    public function instructions(){
		flood_transition(Game.palette[3], function(){
            var instructions = new Instructions(this);
            removeChild(focus);
            addChild(instructions);
            focus = instructions;
        });
    }
	// TODO -- create a Credits class, or credits functionality here
	public function credits() {
		var names:Array<String> = ["Nico Bedford", "Ryan Buckingham", "Ian Humphrey", "Richard Lester", "Justin Liddicoat", "Tanner Stevens"];
//creditsContainer.x = Starling.current.stage.stageWidth/2;
	//creditsContainer.y = Starling.current.stage.stageHeight/5 * 3 + Starling.current.stage.stageHeight/10;
	var i = Starling.current.stage.stageHeight/5 * 3 + Starling.current.stage.stageHeight/10;
	for (n in names) {
		var creditsText:TextField = new TextField(200, 30, n, "Harlow_Solid_Italic_32", 20, 0x000000, false);
		creditsText.color = 0xFFFFFF;
		creditsText.bold = true;
		creditsText.y = i;
		i += 25;
		creditsText.x = Starling.current.stage.stageWidth/2 - creditsText.width/2;
		creditsText.alpha = 0;
		addChild(creditsText);
		Starling.juggler.tween(creditsText, 1.5, {
			alpha: 1,
			delay: 0.5 * names.indexOf(n),
			transition: Transitions.EASE_OUT,
			onComplete: function() {
			    Starling.juggler.tween(creditsText, 1.0, { alpha: 0, transition: Transitions.EASE_OUT,
				   onComplete: function() { removeChild(creditsText); }	});
				}
			});
		}
	}
        }
